# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    if len(password) < 6 or len(password) > 12:
        return False
    contains_lower = False
    contains_upper = False
    contains_special = False
    contains_digit = False
    special_chars = ["$", "!", "@"]
    for i in (password):
        if i.isalpha():
            if i.isupper():
                contains_upper = True
            elif i.islower():
                contains_lower = True
        elif i in special_chars:
            contains_special = True
        elif i.isdigit():
            contains_digit = True
    return contains_upper and contains_lower and contains_special and contains_digit
