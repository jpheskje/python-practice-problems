# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    #return None if list doesn't have enough elements
    if len(values) == 0 or len(values) == 1:
        return None
    #return None if all elements the same
    ##if working with an extremely large dataset and this condition
    ##is effectively impossible than can silence this chunk
    first_element = values[0]
    for i in values:
        if i != first_element:
            stop = False
            break
        else:
            stop = True
    if stop:
        return None
    #return second largest element in list
    result = ""
    values.sort(reverse=True)
    for i, j in enumerate(values):
        if result == "":
            if j > values[i+1]:
                result = values[i+1]
        else:
            break
    result = int(result)
    return result
