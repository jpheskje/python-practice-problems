# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    can_make = []
    for i in ingredients:
        if i == "flour" or i == "eggs" or i == "oil":
            can_make += [True]
        else:
            can_make += [False]
    if can_make[0] == True and can_make[1] == True and can_make[2] == True:
        return True
    else:
        return False
