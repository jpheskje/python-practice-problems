# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if values == []:
        return None
    max = 0
    for i in values:
        if i > max:
            max = i
    return max
