# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

def add_csv_lines(csv_lines):
    #pull out individual strings
    new_list = []
    for num_str in csv_lines: #do all of the following for each indiv csv element
        str_list = num_str.split(",") #perform string split to get list where each num is a str
        temp_ints = [] #initialize list for integers
        for str_val in str_list: #add each integer to list
            temp_ints.append(int(str_val))
        sum_int = 0 #initialize sum of ints
        for i in temp_ints: #sum all ints
            sum_int += i
        new_list.append(sum_int) #add sum to new list
    return new_list
