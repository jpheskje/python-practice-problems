# Write a function that meets these requirements.
#
# Name:       specific_random
# Parameters: none
# Returns:    a random number between 10 and 500, inclusive,
#             that is divisible by 5 and 7
#
# Examples:
#     * returns: 35
#     * returns: 105
#     * returns: 70
#
# Guidance:
#   * Generate all the numbers that are divisible by 5
#     and 7 into a list
#   * Use random.choice to select one

import random

def specific_random():
    div_by_5_7 = random.randint(10,500)
    while ((div_by_5_7 % 5) != 0) or ((div_by_5_7 % 7) != 0):
        div_by_5_7 = random.randint(10,500)
    return div_by_5_7
